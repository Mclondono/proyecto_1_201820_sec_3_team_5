package api;

import java.time.LocalDateTime;
import model.data_structures.IQueue;
import model.data_structures.IDoublyLinkedList;
import model.vo.VOBike;
import model.vo.VOStation;
import model.vo.VOTrip;

public interface IDivvyTripsManager {

    IQueue<VOTrip> A1ViajesEnPeriodoDeTiempo(LocalDateTime fechaInicial, LocalDateTime fechaFinal);
    IDoublyLinkedList<VOBike> A2BicicletasOrdenadasPorNumeroViajes(LocalDateTime fechaInicial, LocalDateTime fechaFinal);
    IDoublyLinkedList<VOTrip> A3ViajesPorBicicletaPeriodoTiempo(int bikeId, LocalDateTime fechaInicial, LocalDateTime fechaFinal);
    IDoublyLinkedList<VOTrip> A4ViajesPorEstacionFinal(int endStationId, LocalDateTime fechaInicial, LocalDateTime fechaFinal);
    IQueue<VOStation> B1EstacionesPorFechaInicioOperacion(LocalDateTime fechaComienzo);
    IDoublyLinkedList<VOBike> B2BicicletasOrdenadasPorDistancia(LocalDateTime fechaInicial, LocalDateTime fechaFinal);
    IDoublyLinkedList<VOTrip> B3ViajesPorBicicletaDuracion(int bikeId, int tiempoMaximo, String genero);
    IDoublyLinkedList<VOTrip> B4ViajesPorEstacionInicial(int startStationId, LocalDateTime fechaInicial, LocalDateTime fechaFinal);
	void C1cargar(String rutaTrips, String rutaStations);

	IQueue<VOTrip> C2ViajesValidadosBicicleta(int bikeId, LocalDateTime fechaInicial, LocalDateTime fechaFinal);

    IDoublyLinkedList<VOBike> C3BicicletasMasUsadas(int topBicicletas);

    IDoublyLinkedList<VOTrip> C4ViajesEstacion(int StationId, LocalDateTime fechaInicial, LocalDateTime fechaFinal);

    public int tripsCargados();

    public int stationsCargadas();
    
}
