package view;

import java.time.LocalDateTime;
import java.util.Scanner;

import controller.Controller;
import model.data_structures.IQueue;
import model.logic.DivvyTripsManager;
import model.data_structures.IDoublyLinkedList;
import model.vo.VOTrip;
import model.vo.VOBike;
import model.vo.VOStation;

public class DivvyTripsManagerView 
{

	public static void main(String[] args)
	{

		Scanner linea = new Scanner(System.in);
		boolean fin = false; 
		Controller elController = new Controller();
		while(!fin)
		{
			
			printMenu();

			int option = linea.nextInt();
			switch(option)
			{

			case 1:  //Carga de datos 1C
				String dataTrips = "";  
				String dataStations = ""; 
				boolean reiniciarDatos = false;
				printMenuCargar();
				int tamanoDatos = linea.nextInt();
				switch (tamanoDatos)
				{
				case 1:
					dataTrips = "./data/Divvy_Trips_2017_Q1.txt";
					dataStations =  "./data/Divvy_Stations_2017_Q1Q2.txt";
					break;
				case 2:
					dataTrips = "./data/Divvy_Trips_2017_Q2.txt";
					dataStations =  "./data/Divvy_Stations_2017_Q1Q2.txt";
					break;
				case 3:
					dataTrips = "./data/Divvy_Trips_2017_Q3.txt";
					dataStations = "./data/Divvy_Stations_2017_Q3Q4.txt";
					break;
				case 4:
					dataTrips = "./data/Divvy_Trips_2017_Q4.txt";
					dataStations = "./data/Divvy_Stations_2017_Q3Q4.txt";
					break;
				case 5: 
					dataTrips = "";
					dataStations = "";
					reiniciarDatos = true;
					break;
				}

				

				
				long startTime = System.currentTimeMillis();


				//Metodo 1C
				Controller.C1cargar(dataTrips, dataStations);
				System.out.println("Total trips cargados en el sistema: " + elController.tripsCargados());
				System.out.println("Total estaciones cargadas en el sistema: " + elController.stationsCargadas());

				long endTime = System.currentTimeMillis();
				long duration = endTime - startTime;

			case 2: // 1A

				System.out.println("Ingrese la fecha inicial (Ej : 28/3/2017)");
				String fechaInicialReq1A = linea.next();

				System.out.println("Ingrese la hora inicial (Ej: 09:00:00)");
				String horaInicialReq1A = linea.next();

				LocalDateTime localDateInicio = convertirFecha_Hora_LDT(fechaInicialReq1A, horaInicialReq1A);					
				if ( localDateInicio == null )
				{
					System.out.println("La informacion no tiene el formato especificado");
					break;
				}

				System.out.println("Ingrese la fecha final (Ej : 29/3/2017)");
				String fechaFinalReq1A = linea.next();

				System.out.println("Ingrese la hora final (Ej: 14:25:30)");
				String horaFinalReq1A = linea.next();

				LocalDateTime localDateFin = convertirFecha_Hora_LDT(fechaFinalReq1A, horaFinalReq1A);
				if ( localDateFin == null )
				{
					System.out.println("La informacion no tiene el formato especificado");
					break;
				}

				IQueue<VOTrip> colaDeViajes = Controller.A1(localDateInicio, localDateFin);
				if(colaDeViajes == null )
				{
					System.out.println("La cola de viajes no ha sido inicializada o no se encontraron viajes en el rango de fechas dado.");
				}
				else
				{
					for(VOTrip v : colaDeViajes)
					{   
						System.out.print("Id VOTrip: " + v.getTripId() + ", ");
						System.out.print("Id bicicleta: "+ v.getBikeId() + ", ");
						System.out.print("Fecha de inicio: "+ v.getStartTime() + ", ");
						System.out.println("Fecha de fin: "+ v.getStopTime());

					}
				}

				break;

			case 3: // 2A
				System.out.println("Ingrese la fecha inicial (Ej : 28/3/2017)");
				String fechaInicialReq2A = linea.next();

				System.out.println("Ingrese la hora inicial (Ej: 09:00:00)");
				String horaInicialReq2A = linea.next();

				LocalDateTime localDateInicio2A = convertirFecha_Hora_LDT(fechaInicialReq2A, horaInicialReq2A);
				if ( localDateInicio2A == null )
				{
					System.out.println("La informacion no tiene el formato especificado");
					break;
				}

				System.out.println("Ingrese la fecha final (Ej : 29/3/2017)");
				String fechaFinalReq2A = linea.next();

				System.out.println("Ingrese la hora final (Ej: 14:25:30)");
				String horaFinalReq2A = linea.next();

				LocalDateTime localDateFin2A = convertirFecha_Hora_LDT(fechaFinalReq2A, horaFinalReq2A);
				if ( localDateFin2A == null )
				{
					System.out.println("La informacion no tiene el formato especificado");
					break;
				}

				//Metodo 2A
				IDoublyLinkedList<VOBike> bicicletasOrdenadas2A = Controller.A2(localDateInicio2A, localDateFin2A);
				if( bicicletasOrdenadas2A == null || bicicletasOrdenadas2A.size() == 0)
				{
					System.out.println("La cola de bicicletas no ha sido inicializada o no se encontraron bicicletas en el rango de fechas dado.");
				}
				else
				{
					for(VOBike b : bicicletasOrdenadas2A)
					{
						System.out.print("Bicicleta Id: " + b.getVOBikeId() + ", ");
						System.out.print("Total de viajes: "+ b.getTotalVOTrips() + ", ");
						System.out.println("Total distancia recorrida: "+ b.getTotalDistance());
						System.out.println("-----");
					}
				}

				break;

			case 4: //3A
				System.out.println("Ingrese el id de la Bicleta: ");

				try
				{
					int idBicicleta3A = Integer.parseInt(linea.next());			

					System.out.println("Ingrese la fecha inicial (Ej : 28/3/2017)");
					String fechaInicialReq3A = linea.next();

					System.out.println("Ingrese la hora inicial (Ej: 09:00:00)");
					String horaInicialReq3A = linea.next();

					LocalDateTime localDateInicio3A = convertirFecha_Hora_LDT(fechaInicialReq3A, horaInicialReq3A);
					if ( localDateInicio3A == null )
					{
						System.out.println("La informacion no tiene el formato especificado");
						break;
					}

					System.out.println("Ingrese la fecha final (Ej : 29/3/2017)");
					String fechaFinalReq3A = linea.next();

					System.out.println("Ingrese la hora final (Ej: 14:25:30)");
					String horaFinalReq3A = linea.next();

					LocalDateTime localDateFin3A = convertirFecha_Hora_LDT(fechaFinalReq3A, horaFinalReq3A);
					if ( localDateFin3A == null )
					{
						System.out.println("La informacion no tiene el formato especificado");
						break;
					}

					//Metodo 3A
					IDoublyLinkedList<VOTrip> viajesPorBicicleta = elController.A3(idBicicleta3A, localDateInicio3A, localDateFin3A);
					if ( viajesPorBicicleta == null || viajesPorBicicleta.size() == 0)
					{
						System.out.println("La lista de viajes no ha sido inicializada o no se encontraron viajes en el rango de fechas dado.");
					}

					else
					{
						for(VOTrip v : viajesPorBicicleta)
						{
							System.out.print("VOTrip id: "+ v.getTripId() + ", ");
							System.out.print("Fecha Inicio: "+ v.getStartTime() + ", ");
							System.out.println("Fecha Fin: "+ v.getStopTime());
						}
					}

				}

				catch (Exception e) 
				{
					System.out.println("El id de la bicicleta proporciado no corresponde a un dato numerico");
				}
				break;

			case 5: // 4A
				System.out.println("Ingrese id de la estacion final: ");

				try
				{
					int idEstacionFinal4A = Integer.parseInt(linea.next());

					System.out.println("Ingrese la fecha inicial (Ej : 28/3/2017)");
					String fechaInicialReq4A = linea.next();

					System.out.println("Ingrese la hora inicial (Ej: 09:00:00)");
					String horaInicialReq4A = linea.next();

					LocalDateTime localDateInicio4A = convertirFecha_Hora_LDT(fechaInicialReq4A, horaInicialReq4A);
					if ( localDateInicio4A == null )
					{
						System.out.println("La informacion no tiene el formato especificado");
						break;
					}

					System.out.println("Ingrese la fecha final (Ej : 29/3/2017)");
					String fechaFinalReq4A = linea.next();

					System.out.println("Ingrese la hora final (Ej: 14:25:30)");
					String horaFinalReq4A = linea.next();

					LocalDateTime localDateFin4A = convertirFecha_Hora_LDT(fechaFinalReq4A, horaFinalReq4A);
					if ( localDateFin4A == null )
					{
						System.out.println("La informacion no tiene el formato especificado");
						break;
					}

					//Metodo 4A
					IDoublyLinkedList<VOTrip> viajesEstacionFinal = elController.A4(idEstacionFinal4A, localDateInicio4A, localDateFin4A);
					if( viajesEstacionFinal == null || viajesEstacionFinal.size() == 0)
					{
						System.out.println("La lista de viajes no ha sido inicializada o no se encontraron viajes en el rango de fechas dado.");
						break;
					}
					for(VOTrip v : viajesEstacionFinal)
					{
						System.out.print("VOTrip ID: "+ v.getTripId() + ", ");
						System.out.print("VOTrip ID: "+ v.getBikeId() + ", ");
						System.out.println("Fecha terminacion: " + v.getStopTime());
						System.out.println("-----");
					}
				}
				catch (Exception e) 
				{
					System.out.println("El id de la estacion proporciado no corresponde a un dato numerico");
				}

				break;

			case 6: // 1B
				try{
					System.out.println("Ingrese la fecha inicial (Ej : 28/3/2017)");
					String fechaInicialReq1B = linea.next();

					System.out.println("Ingrese la hora inicial (Ej: 09:00:00)");
					String horaInicialReq1B = linea.next();

					LocalDateTime localDateInicio1B = convertirFecha_Hora_LDT(fechaInicialReq1B, horaInicialReq1B);

					IQueue<VOStation> estacionesFechaInicio = elController.B1(localDateInicio1B);
					if(estacionesFechaInicio.isEmpty())
					{
						System.out.println("No se han encontrado datos bajo esas especificaciones");
						break;
					}
					for(VOStation s : estacionesFechaInicio)
					{
						System.out.print("Estacion id: "+ s.getStationId() + ", ");
						System.out.print("Estacion Nombre: "+ s.getStationName() + ", ");
						System.out.println("Fecha de Inicio Operaciones: "+ s.getStartDate());
						System.out.println("-----");
					}
				} 
				catch(Exception e)
				{
					System.out.println("El formato no es el esperado intente de nuevo \n");
					break;
				}
				break;
				

			case 7: //Req 2B
				//Fecha Inicial
				try{
					System.out.println("Ingrese la fecha inicial (Ej : 28/3/2017)");

					String fechaInicialReq2B = linea.next();

					//Hora inicial
					System.out.println("Ingrese la hora inicial (Ej: 09:00:00)");
					String horaInicialReq2B = linea.next();

					// Datos Fecha/Hora inicial
					LocalDateTime localDateInicio2B = convertirFecha_Hora_LDT(fechaInicialReq2B, horaInicialReq2B);

					//fecha final
					System.out.println("Ingrese la fecha final (Ej : 29/3/2017)");
					String fechaFinalReq2B = linea.next();

					//hora final
					System.out.println("Ingrese la hora final (Ej: 14:25:30)");
					String horaFinalReq2B = linea.next();

					// Datos Fecha/Hora final
					LocalDateTime localDateFin2B = convertirFecha_Hora_LDT(fechaFinalReq2B, horaFinalReq2B);

					//Metodo 2B
					IDoublyLinkedList<VOBike> bicicletasOrdenadasPorDistancia = elController.B2(localDateInicio2B, localDateFin2B);
					if(bicicletasOrdenadasPorDistancia.isEmpty())
					{
						System.out.println("No se han encontrado datos bajo esas especificaciones");
						break;
					}
					for(VOBike b : bicicletasOrdenadasPorDistancia)
					{
						System.out.print("Bicicleta Id: "+ b.getVOBikeId() + ", ");
						System.out.print("Distancia Total: " + b.getTotalDistance() + ", ");
						System.out.println("Viajes Totales: " + b.getTotalVOTrips());
						System.out.println("-----");
					}
				} 
				catch(Exception e)
				{
					System.out.println("El formato no es el esperado intente de nuevo \n");
					break;
				}
				break;

			case 8: //Req 3B
				System.out.println("Ingrese bicicleta Id:");
				try{
					int bicicletaId3B = Integer.parseInt(linea.next());

					System.out.println("Ingrese tiempo maximo");
					int tiempoMaximo3B = Integer.parseInt(linea.next());

					System.out.println("Ingrese genero");
					String genero3B = linea.next();

					IDoublyLinkedList<VOTrip> viajesporBicicletaDuracion = elController.B3(bicicletaId3B, tiempoMaximo3B, genero3B);

					if(viajesporBicicletaDuracion.isEmpty())
					{
						System.out.println("No se han encontrado datos bajo esas especificaciones");
						break;
					}
					for(VOTrip t : viajesporBicicletaDuracion)
					{
						System.out.print("VOTrip Id: "+ t.getTripId() + ", ");
						System.out.print("Fecha inicial: "+ t.getStartTime() + ", ");
						System.out.print("Fecha final: "+ t.getStopTime() + ", ");
						System.out.println("Duracion viaje: "+ t.getTripDuration());
						System.out.println("-----");
					}
				}
				catch(NumberFormatException e)
				{
					System.out.println("El formato del dato ingresado no es un numero"); 
					break;
				}
				break;

			case 9: //Req 4B
				try{
					System.out.println("Ingrese identificador estacion: ");
				
				int estacionInicioId = Integer.parseInt(linea.next());

				//Fecha Inicial
				System.out.println("Ingrese la fecha inicial (Ej : 28/3/2017)");
				String fechaInicialReq4B = linea.next();

				//Hora inicial
				System.out.println("Ingrese la hora inicial (Ej: 09:00:00)");
				String horaInicialReq4B = linea.next();

				// Datos Fecha/Hora inicial
				LocalDateTime localDateInicio4B = convertirFecha_Hora_LDT(fechaInicialReq4B, horaInicialReq4B);

				//fecha final
				System.out.println("Ingrese la fecha final (Ej : 29/3/2017)");
				String fechaFinalReq4B = linea.next();

				//hora final
				System.out.println("Ingrese la hora final (Ej: 14:25:30)");
				String horaFinalReq4B = linea.next();

				// Datos Fecha/Hora final
				LocalDateTime localDateFin4B = convertirFecha_Hora_LDT(fechaFinalReq4B, horaFinalReq4B);

				IDoublyLinkedList<VOTrip> ViajesporEstacionInicial = elController.B4(estacionInicioId, localDateInicio4B, localDateFin4B);
				if(ViajesporEstacionInicial.isEmpty())
				{
					System.out.println("No se han encontrado datos bajo esas especificaciones");
					break;
				}
				for(VOTrip t : ViajesporEstacionInicial)
				{
					System.out.print("VOTrip Id: "+ t.getTripId() + ", ");
					System.out.print("VOTrip Id: "+t.getBikeId() + ", ");
					System.out.println("Fecha Inicio: " + t.getStartTime());
					System.out.println("-----");
				}
				}
				catch(Exception e)
				{
					System.out.println("El formato del dato ingresado no es un numero"); 
					break;
				}
				break;

			case 10: //Req 2C
				System.out.println("Ingrese identificador bicicleta: ");
				int bicicletaId = Integer.parseInt(linea.next());

				//Fecha Inicial
				System.out.println("Ingrese la fecha inicial (Ej : 28/3/2017)");
				String fechaInicialReq2C = linea.next();

				//Hora inicial
				System.out.println("Ingrese la hora inicial (Ej: 09:00:00)");
				String horaInicialReq2C = linea.next();

				// Datos Fecha/Hora inicial
				LocalDateTime localDateInicio2C = convertirFecha_Hora_LDT(fechaInicialReq2C, horaInicialReq2C);
				if ( localDateInicio2C == null )
				{
					System.out.println("La informacion no tiene el formato especificado");
					break;
				}

				//fecha final
				System.out.println("Ingrese la fecha final (Ej : 2017-02-01)");
				String fechaFinalReq2C = linea.next();

				//hora final
				System.out.println("Ingrese la hora final (Ej: 14:25:30)");
				String horaFinalReq2C = linea.next();

				// Datos Fecha/Hora final
				LocalDateTime localDateFin2C = convertirFecha_Hora_LDT(fechaFinalReq2C, horaFinalReq2C);
				if ( localDateFin2C == null )
				{
					System.out.println("La informacion no tiene el formato especificado");
					break;
				}
				// Metodo
				IQueue<VOTrip> viajesValidados2C = elController.C2ViajesValidadosBicicleta(bicicletaId, localDateInicio2C, localDateFin2C);
				if( viajesValidados2C != null )
				{
					while(!viajesValidados2C.isEmpty())
					{
						VOTrip valido = viajesValidados2C.dequeue();
						System.out.print("Tiempo de terminacion: " + valido.getStopTime() + ", ");
						System.out.print("Estacion de terminacion: " + valido.getEndStationId() + ", ");
						VOTrip invalido = viajesValidados2C.dequeue();
						System.out.print("Tiempo de inicio: "+ invalido.getStartTime() + ", ");
						System.out.println("Estacion de inicio: "+ invalido.getStartStationId());
						System.out.println("-----");
					}
				}
				else
				{
					System.out.println("No se encontraron viajes para validar.");
				}

				break;

			case 11: //Req 3C
				System.out.println("Ingrese numero de bicicletas: ");
				try
				{
					int numeroBicicletas3C = Integer.parseInt(linea.next());

					IDoublyLinkedList<VOBike> topBicicletas = elController.C3BicicletasMasUsadas(numeroBicicletas3C);
					if( topBicicletas != null )
					{
						for(VOBike b : topBicicletas)
						{
							System.out.print("VOTrip Id: "+ b.getVOBikeId() + ", ");
							System.out.println("Duracion total del viaje: " + b.getTotalDuration());
						}
					}
					else
					{
						System.out.println("No hay bicicletas en el sistema");
					}

				}
				catch (Exception e) 
				{
					System.out.println("El dato ingresado no es numerico");
				}

				break;

			case 12: //Req 4C
				System.out.println("Ingrese Id de estacion: ");
				int idEstacion4C = Integer.parseInt(linea.next());

				//Fecha Inicial
				System.out.println("Ingrese la fecha inicial (Ej : 28/3/2017)");
				String fechaInicialReq4C = linea.next();

				//Hora inicial
				System.out.println("Ingrese la hora inicial (Ej: 09:00:00)");
				String horaInicialReq4C = linea.next();

				// Datos Fecha/Hora inicial
				LocalDateTime localDateInicio4C = convertirFecha_Hora_LDT(fechaInicialReq4C, horaInicialReq4C);

				//fecha final
				System.out.println("Ingrese la fecha final (Ej : 2017-02-01)");
				String fechaFinalReq4C = linea.next();

				//hora final
				System.out.println("Ingrese la hora final (Ej: 14:25:30)");
				String horaFinalReq4C = linea.next();

				// Datos Fecha/Hora final
				LocalDateTime localDateFin4C = convertirFecha_Hora_LDT(fechaFinalReq4C, horaFinalReq4C);

				// Metodo
				IDoublyLinkedList<VOTrip> viajesDeEstacion4C = elController.C4ViajesEstacion(idEstacion4C, localDateInicio4C, localDateFin4C);
				for( VOTrip t : viajesDeEstacion4C)
				{
					System.out.print("VOTrip Id: "+ t.getTripId() + ", ");
					System.out.print("VOTrip Id: "+ t.getBikeId() + ", ");

					//TODO Completar: Informar si el viaje inicia en la estacion y su tiempo de inicio (t.getStartTime()) o 
					if(!t.isStationfC4())
						System.out.println("Viaje de incio, "+t.getStartTime());
					else
						System.out.println("Viaje de llegada, "+t.getStopTime());
					t.changec4(false);
					//TODO Completar: Informar si el viaje termina en la estacion y su tiempo de final (t.getStopTime()) 
					System.out.println("-----");
				}
				break;

			case 13: //Salir
				fin = true;
				linea.close();
				break;
				
			}
		}
	}

	private static void printMenu()
	{
		System.out.println("---------ISIS 1206 - Estructuras de Datos----------");
		System.out.println("-------------------- Proyecto 1   - 2018-2 ----------------------");
		System.out.println("Iniciar la Fuente de Datos a Consultar :");
		System.out.println("1. Actualizar la informacion del sistema con una fuente de datos (2017-Q1, 2017-Q2, 2017-Q3, 2017-Q4)");

		System.out.println("\nParte A:\n");
		System.out.println("2. Obtener la cola con todos los viajes de una bicicleta en rango de fecha (1A)");
		System.out.println("3. Obtener las bicicletas ordenadas de mayor a menor por el numero de viajes realizados (2A)");
		System.out.println("4. Obtener los viajes de una bicicleta en un rango de fecha dado (3A)");
		System.out.println("5. Obtener viajes que terminaron en una estacion (4A)");

		System.out.println("\nParte B:\n");
		System.out.println("6. Obtener Cola con las estaciones que comenzaron despues de una fecha (1B)");
		System.out.println("7. Bicicletas ordenadas por distancia total recorrida en un rango de fecha (2B)");
		System.out.println("8. Viajes de una bicicleta con duracion menor a una dada en un rango de fecha (3B)");
		System.out.println("9. Viajes que iniciaron en una estacion en un rango dado. (4B)");


		System.out.println("\nParte C:\n");
		System.out.println("10. Validar viajes de una bicicleta (2C)");
		System.out.println("11. Bicicletas mas usadas segun duracion de viaje (3C)");
		System.out.println("12. Viajes que iniciaron en una estacion. (4C)");
		System.out.println("13. Salir");
		System.out.println("Ingrese el numero de la opcion seleccionada y presione <Enter> para confirmar: (e.g., 1):");

	}

	private static void printMenuCargar()
	{
		System.out.println("-- Que fuente de datos desea agregar a los datos del sistema (carga incremental)?");
		System.out.println("-- 1. 2017-Q1");
		System.out.println("-- 2. 2017-Q2");
		System.out.println("-- 3. 2017-Q3");
		System.out.println("-- 4. 2017-Q4");
		System.out.println("-- 5. Reiniciar datos del sistema");		
		System.out.println("-- Ingrese el numero de la fuente a cargar y presione <Enter> para confirmar: (e.g., 1)");
	}

	/**
	 * Convertir fecha y hora a un objeto LocalDateTime
	 * @param fecha fecha en formato dd/mm/aaaa con dd para dia, mm para mes y aaaa para agno
	 * @param hora hora en formato hh:mm:ss con hh para hora, mm para minutos y ss para segundos
	 * @return objeto LDT con fecha y hora integrados
	 */
	private static LocalDateTime convertirFecha_Hora_LDT(String fecha, String hora) throws NumberFormatException
	{
		String[] datosFecha = fecha.split("/");
		String[] datosHora = hora.split(":");

		try
		{
			int agno = Integer.parseInt(datosFecha[2]);
			int mes = Integer.parseInt(datosFecha[1]);
			int dia = Integer.parseInt(datosFecha[0]);
			int horas = Integer.parseInt(datosHora[0]);
			int minutos = Integer.parseInt(datosHora[1]);
			int segundos = Integer.parseInt(datosHora[2]);

			return LocalDateTime.of(agno, mes, dia, horas, minutos, segundos);
		}
		catch (Exception e) 
		{
			return null;
		}

	}

}
