package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Iterator;

import api.IDivvyTripsManager;
import model.data_structures.Queue;
import model.data_structures.IQueue;
import model.data_structures.IComparator;
import model.data_structures.IDoublyLinkedList;
import model.data_structures.IStack;
import model.data_structures.DoublyLinkedList;
import model.data_structures.MergeSort;
import model.data_structures.Stack;
import model.data_structures.QuickSort;
import model.vo.VOBike;
import model.vo.VOStation;
import model.vo.VOTrip;

public class DivvyTripsManager implements IDivvyTripsManager {
	
	private static final int EARTH_RADIUS = 6371;

	



	IDoublyLinkedList<VOTrip> listVOTrips;

	
	IDoublyLinkedList<VOStation> listVOStations;


	IDoublyLinkedList<VOBike> listVOBikes;


	private int VOTripsCargados;


	private int VOStationCargadas;


	boolean q1, q2, q3, q4;


	public DivvyTripsManager()
	{
		listVOTrips = new Queue<VOTrip>();
		listVOStations = new Queue<VOStation>();
		listVOBikes = new Queue<VOBike>();
		q1 = false;
		q2 = false;
		q3 = false;
		q4 = false;
		VOTripsCargados = 0;
		VOStationCargadas = 0;
	}

	public IQueue<VOTrip> A1ViajesEnPeriodoDeTiempo(LocalDateTime fechaInicial, LocalDateTime fechaFinal)
	{
		IQueue<VOTrip> Queue = new Queue<VOTrip>();

		if( listVOTrips.size() == 0 )
		{
			return null;
		}
		Iterator<VOTrip> iter = listVOTrips.iterator();

		while( iter.hasNext() )
		{
			VOTrip actual = (VOTrip) iter.next();
			if( actual.getStartTime().isAfter(fechaInicial) && actual.getStopTime().isBefore(fechaFinal))
			{
				Queue.enqueue(actual);
			}
			if( actual.getStartTime().isAfter(fechaFinal))
			{
				break;
			}
		}
		return Queue;
	}

	public IDoublyLinkedList<VOBike> A2BicicletasOrdenadasPorNumeroViajes(LocalDateTime fechaInicial, LocalDateTime fechaFinal) 
	{
		IDoublyLinkedList<VOBike> DoublyLinkedList = new DoublyLinkedList<VOBike>();

		if( listVOBikes.size() == 0 )
		{
			return null;
		}
		Iterator<VOBike> iter = listVOBikes.iterator();

		while( iter.hasNext() )
		{
			VOBike actual = (VOBike) iter.next();
			if( actual.viajesPorFecha(fechaInicial, fechaFinal).size() > 0)
			{
				DoublyLinkedList.add(actual);
			}
		}
		IComparator<VOBike> comparador = new VOBike.ComparadorVOBikeXNumeroViajes();
		MergeSort<VOBike> merge = new MergeSort<VOBike>();
		merge.mergeSort(DoublyLinkedList.getFirst(), comparador);

		return DoublyLinkedList;
	}

	public IDoublyLinkedList<VOTrip> A3ViajesPorBicicletaPeriodoTiempo(int VOBikeId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) 
	{
		if( listVOBikes.size() == 0 )
		{
			return null;
		}
		VOBike VOBike = findVOBike(VOBikeId);	
		if( VOBike != null )
		{
			IDoublyLinkedList<VOTrip> VOTrips = VOBike.viajesPorFecha(fechaInicial, fechaFinal);
			QuickSort<VOTrip> quick = new QuickSort<VOTrip>();
			quick.quickSort((VOTrips.getFirst()));
			return VOTrips;
		}
		else
		{
			return null;
		}
	}



	public IDoublyLinkedList<VOTrip> A4ViajesPorEstacionFinal(int endVOStationId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) 
	{
		IDoublyLinkedList<VOTrip> VOTrips = new DoublyLinkedList<VOTrip>();

		if( listVOTrips.size() == 0 )
		{
			return null;
		}

		Iterator<VOTrip> iter = listVOTrips.iterator();
		while( iter.hasNext() )
		{
			VOTrip actual = iter.next();
			if( actual.getStopTime().isAfter(fechaInicial) && actual.getStopTime().isBefore(fechaFinal) 
					&& actual.getEndStationId() == endVOStationId )
			{
				VOTrips.add(actual);
			}

			// Al estar ordenados por su fecha inicial, si un viaje empieza despues de la hora del 
			// rango dado, ese y los demas viajes no cumplen con el parametro
			if( actual.getStartTime().isAfter(fechaFinal))
			{
				break;
			}
		}
		IComparator<VOTrip> comparador = new VOTrip.ComparadorXHoraEnd();
		MergeSort<VOTrip> merge = new MergeSort<VOTrip>();
		merge.mergeSort(VOTrips.getFirst(), comparador);

		return VOTrips;
	}

	public IQueue<VOStation> B1EstacionesPorFechaInicioOperacion(LocalDateTime fechaComienzo) 
	{
		IQueue<VOStation> retornable = new Queue<VOStation>();
		//controla si esta luego de la fecha dada
		boolean mayor =false;
		for (VOStation VOStation : listVOStations) 
		{
			if(VOStation.getStartDate().isAfter(fechaComienzo)&& !mayor)
				mayor = true;
			if(mayor)
				retornable.enqueue(VOStation);
		}
		return retornable;

	}

	public IDoublyLinkedList<VOBike> B2BicicletasOrdenadasPorDistancia(LocalDateTime fechaInicial, LocalDateTime fechaFinal) 
	{
		IDoublyLinkedList<VOBike> retornable = new DoublyLinkedList<VOBike>();
		for (VOBike VOBike : listVOBikes) 
		{
			//cambia la distancia por la que existe dentro del rango
			VOBike.viajesPorFecha(fechaInicial, fechaFinal);
			if(VOBike.getTotalDistance()>0)
				retornable.add(VOBike);
		}
		IComparator<VOBike> compXdist = new VOBike.ComparadorVOBikeXDistancia();
		MergeSort<VOBike> merge = new MergeSort<VOBike>();
		merge.mergeSort(retornable.getFirst(), compXdist);
		if(!retornable.isEmpty())
				invertir(retornable);

		return retornable;
	}

	public IDoublyLinkedList<VOTrip> B3ViajesPorBicicletaDuracion(int VOBikeId, int tiempoMaximo, String genero) 
	{
		IDoublyLinkedList<VOTrip> retornable = new DoublyLinkedList<VOTrip>();
		//para la bicicleta del ID se comprueban las condiciones en cada viaje
		for (VOTrip VOTrip : findVOBike(VOBikeId).getListaViajes()) {
			if(VOTrip.getTripDuration()<tiempoMaximo&&VOTrip.getGender().equalsIgnoreCase(genero))
				retornable.add(VOTrip);
		}
		invertir(retornable);

		return retornable;
	}

	public IDoublyLinkedList<VOTrip> B4ViajesPorEstacionInicial(int startVOStationId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) 
	{
		IDoublyLinkedList<VOTrip> retornable = new DoublyLinkedList<VOTrip>();
		boolean rango = false;
		for (VOTrip VOTrip : listVOTrips)
		{
			if(VOTrip.getStartTime().isAfter(fechaInicial))
				rango =true;
			if(VOTrip.getStartStationId()==startVOStationId&& rango)
				retornable.addFirst(VOTrip);
			if(VOTrip.getStartTime().isAfter(fechaFinal))
			{
				break;
			}
		}
		invertir(retornable);
		return retornable;
	}

	public void C1cargar(String rutaVOTrips, String rutaVOStations) 
	{
		// Reinicia los datos del sistema
		if( rutaVOTrips == "" && rutaVOStations == "")
		{
			q1 = q2 = q3 = q4 = false;
			listVOTrips = new DoublyLinkedList<VOTrip>();
			listVOBikes = new DoublyLinkedList<VOBike>();
			listVOStations = new DoublyLinkedList<VOStation>();
			VOTripsCargados = VOStationCargadas = 0;
		}

		else if(rutaVOTrips.contains("Q1") && !q1)
		{
			if( !(q1 || q2) )
			{
				loadStations(rutaVOStations);
				q1 = true;
			}
			loadTrips(rutaVOTrips);
		}
		else if(rutaVOTrips.contains("Q2") && !q2)
		{
			if( !(q1 || q2) )
			{
				loadStations(rutaVOStations);
				q2 = true;
			}
			loadTrips(rutaVOTrips);
		}

		else if(rutaVOTrips.contains("Q3") && !q3)
		{
			if( !(q3 || q4) )
			{
				loadStations(rutaVOStations);
				q3 = true;
			}
			loadTrips(rutaVOTrips);
		}
		else if(rutaVOTrips.contains("Q4") && !q4)
		{
			if( !(q3 || q4) )
			{
				loadStations(rutaVOStations);
				q4 = true;
			}
			loadTrips(rutaVOTrips);
		}
	}


	public IQueue<VOTrip> C2ViajesValidadosBicicleta(int VOBikeId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) 
	{
		IDoublyLinkedList<VOTrip> VOTrips = A3ViajesPorBicicletaPeriodoTiempo(VOBikeId, fechaInicial, fechaFinal);
		IQueue<VOTrip> Queue = new Queue<VOTrip>();
		if( VOTrips != null )
		{
			Iterator<VOTrip> iter = VOTrips.iterator();
			IStack<VOTrip> StackValidacion = new Stack<VOTrip>();
			while( iter.hasNext() )
			{
				VOTrip aValidar = iter.next();
				if( StackValidacion.isEmpty() )
				{
					StackValidacion.push(aValidar);
				}
				else
				{
					VOTrip recuperado = StackValidacion.pop();
					// Validacion
					if( recuperado.getEndStationId() == aValidar.getStartStationId() )
					{
						StackValidacion.push(recuperado);
						StackValidacion.push(aValidar);
					}
					// Inconsistencia
					else
					{
						Queue.enqueue(recuperado);
						Queue.enqueue(aValidar);
						StackValidacion = new Stack<VOTrip>();
						StackValidacion.push(aValidar);
					}
				}
			}
			return Queue;
		}
		else
		{
			return null;	
		}
	}

	public IDoublyLinkedList<VOBike> C3BicicletasMasUsadas(int topBicicletas) 
	{
		IDoublyLinkedList<VOBike> VOBikes = new DoublyLinkedList<VOBike>();
		IDoublyLinkedList<VOBike> VOBikes2 = new DoublyLinkedList<VOBike>();

		if( listVOBikes.size() == 0 )
		{
			return null;
		}
		else
		{
			Iterator<VOBike> iter = listVOBikes.iterator();
			while( iter.hasNext() )
			{
				VOBike actual = iter.next();
				actual.calcularTotalDeUso();
				VOBikes.add(actual);
			}
			IComparator<VOBike> comparador = new VOBike.ComparadorVOBikeXTotalUso();
			MergeSort<VOBike> merge = new MergeSort<VOBike>();
			merge.mergeSort(VOBikes.getFirst(), comparador);

			int i = 0;
			iter = VOBikes.iterator();
			while( iter.hasNext() && i < topBicicletas )
			{
				VOBikes2.add(iter.next());
				i++;
			}
			return VOBikes2;
		}
	}

	public IDoublyLinkedList<VOTrip> C4ViajesEstacion(int VOStationId, LocalDateTime fechaInicial, LocalDateTime fechaFinal){
		IDoublyLinkedList<VOTrip> retornable = new DoublyLinkedList<VOTrip>();
		for (VOTrip VOTrip : listVOTrips) {
			if (VOTrip.getStartStationId()==VOStationId&&VOTrip.getStartTime().isAfter(fechaInicial)
					&&VOTrip.getStartTime().isBefore(fechaFinal))
				retornable.add(VOTrip);
			if(VOTrip.getEndStationId()==VOStationId&&VOTrip.getStopTime().isAfter(fechaInicial)
					&&VOTrip.getStopTime().isBefore(fechaFinal))
			{
				VOTrip.changec4(true);
				retornable.add(VOTrip);
			}
		}
		MergeSort<VOTrip> merge = new MergeSort<VOTrip>();
		IComparator<VOTrip> horaIxHoraf = new VOTrip.ComparadorXHoraEndXhoraInical();
		merge.mergeSort(retornable.getFirst(), horaIxHoraf);
		return retornable;
	}

	/**
	 * VOTrips que se encuentran cargados en el sistema
	 * @return VOTripsCargados
	 */
	public int VOTripsCargados()
	{
		return VOTripsCargados;
	}

	/**
	 * VOStations que se encuentran cargadas en el sistema
	 * @return VOStationCargadas
	 */
	public int VOStationsCargadas()
	{
		return VOStationCargadas;
	}

	/**
	 * Cambia el orden como se encuentra la Queue
	 * @param Queue Queue a la cual se le invierte el orden
	 */
	private void invertir( IDoublyLinkedList DoublyLinkedList )
	{
		IStack Stack = new Stack();
		
		while( !DoublyLinkedList.isEmpty() )
		{
			Stack.push( DoublyLinkedList.deleteFirst().get());
		}
		while( !Stack.isEmpty() )
		{
			DoublyLinkedList.add(Stack.pop());
		}
	}

	private VOBike findVOBike( int id )
	{
		VOBike VOBike = null;

		Iterator<VOBike> iter = listVOBikes.iterator();
		while( iter.hasNext() )
		{
			VOBike actual =  iter.next();
			if( actual.getVOBikeId() == id )
			{
				VOBike = actual;
				break;
			}
		}
		return VOBike;
	}

	/**
	 * <prev> La Queue de VOTrips ha sido inicializada
	 * Carga los VOTrips de un archivo dado
	 * @param rutaVOTrips Ruta del archivo con los VOTrips a cargar
	 */
	private void quick() {
		QuickSort<VOTrip> quick = new QuickSort<VOTrip>();
		quick.quickSort((listVOTrips.getLast()));
		for (VOTrip VOTrip : listVOTrips) {
			VOBike VOBike = null;
			boolean hayVOBike= false;
			if(listVOBikes.isEmpty())
			{
				VOBike = new VOBike(VOTrip.getBikeId());
				listVOBikes.addFirst(VOBike);
			}
			Iterator<VOBike> iter = listVOBikes.iterator();
			while( iter.hasNext() && !hayVOBike)
			{
				VOBike sd =  iter.next();
				if ( sd.getVOBikeId()==VOTrip.getBikeId() )
				{
					sd.anadirviaje(VOTrip);
					hayVOBike = true;
				}
			}
			if(!hayVOBike)
			{
				VOBike = new VOBike(VOTrip.getBikeId());
				listVOBikes.addFirst(VOBike);
				VOBike.anadirviaje(VOTrip); 
			}


		}
	}
	


	public void loadTrips (String tripsFile)  {
		try{
			File f = new File(tripsFile);
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);
			String line = br.readLine();
			line = br.readLine();

			while(line != null)
			{
				VOTrip vo = crearVOTrip(line);
				listVOTrips.addFirst(vo);
				VOTripsCargados++;
				line = br.readLine();
			}
			
			System.out.println("Cantidad de trips cargados: " + VOTripsCargados);
			
	}
		
	catch (Exception e) {
		System.out.println(e.getMessage());
		e.printStackTrace();
	}
		quick();
	}

	
	
		public void loadStations (String stationsFile) {
		
		try{
				File f = new File(stationsFile);
				FileReader fr = new FileReader(f);
				BufferedReader br = new BufferedReader(fr);
				String line = br.readLine();
				line = br.readLine();
				while(line != null )
				{
					VOStation VOStation = crearVOStation(line);
					listVOStations.addFirst(VOStation);
					VOStationCargadas++;
					line = br.readLine();
				}
				System.out.println("Cantidad de estaciones cargada: " + VOStationCargadas);

			
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
	}



	/**
	 * Crea un nuevo VOTrip
	 * @param line String con la informacion del viaje
	 * @return VOTrip con las especificaciones dadas
	 */
	private VOTrip crearVOTrip(String line) 
	{
		String[] informacion = line.split(",");
		String VOTripId = informacion[0].replace("\"", "");
		String startTime = informacion[1].replace("\"", "");
		String stopTime = informacion[2].replace("\"", "");
		String VOBikeId = informacion[3].replace("\"", "");
		String VOTripDuration = informacion[4].replace("\"", "");
		int startVOStationId = Integer.parseInt( informacion[5].replace("\"", "")  );
		int endVOStationId = Integer.parseInt( informacion[7].replace("\"", "")  );
		String gender;
		if(informacion.length==10||!(informacion[10].contains("male")||informacion[10].contains("Male")))
		{
			gender = VOTrip.UNKNOWN;
		}
		else if( informacion[10].contains("fe")||informacion[10].contains("Fe"))
		{
			gender = VOTrip.FEMALE;
		}
		else
		{
			gender = VOTrip.MALE;
		}
		Double distance = calcularDistancia(startVOStationId, endVOStationId);

		VOTrip VOTrip = new VOTrip(VOTripId, startTime, stopTime, VOBikeId, VOTripDuration, startVOStationId, endVOStationId, gender, distance);
		return VOTrip;
	}

	/**
	 * Encuentra un estacion dentro de la DoublyLinkedList de estaciones dado su id
	 * @param id ID de la estacion
	 * @return VOStation con el ID dado
	 */
	private VOStation findVOStation( int id )
	{
		VOStation VOStation = null;
		//		Falta implementacion
		Iterator<VOStation> iter = listVOStations.iterator();
		//	Falta implementacion
		while( iter.hasNext() )
		{
			VOStation sd = (VOStation) iter.next();
			if ( sd.getStationId() == id )
			{
				VOStation = sd;
				break;
			}
		}

		return VOStation;
	}

	/**
	 * Calcula la distancia entre dos estaciones dado sus IDs
	 * @param initialVOStationId ID de la estaccion de partida
	 * @param endVOStationId ID de la estacion de llegada
	 * @return Distancia entre las estaciones
	 */
	private Double calcularDistancia(int initialVOStationId, int endVOStationId)
	{
		VOStation initial = findVOStation(initialVOStationId);
		VOStation end = findVOStation(endVOStationId);
		Double startLat = initial.getLatitude();
		Double startLong = initial.getLongitude();
		Double endLat = end.getLatitude();
		Double endLong = end.getLongitude();

		Double dLat  = Math.toRadians((endLat - startLat));
		Double dLong = Math.toRadians((endLong - startLong));

		startLat = Math.toRadians(startLat);
		endLat   = Math.toRadians(endLat);

		Double a = haversin(dLat) + Math.cos(startLat) * Math.cos(endLat) * haversin(dLong);
		Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

		return EARTH_RADIUS * c;
	}

	private static double haversin(double val) 
	{
		return Math.pow(Math.sin(val / 2), 2);
	}

	/**
	 * Crea una estacion dada su informacion
	 * @param line Informacion de la estacion
	 * @return VOStation con las especificaciones dadas
	 */
	private VOStation crearVOStation(String line) 
	{
		String[] information = line.split(",");
		String id = information[0].replace("\"", "");
		String name = information[1].replace("\"", "");
		String latitud = information[3].replace("\"", "");
		String longitud = information[4].replace("\"", "");
		String capacity = information[5].replace("\"", "");
		String date = information[6].replace("\"", "");

		VOStation VOStation = new VOStation(id, name, latitud, longitud, capacity, date);

		return VOStation;
	}

	@Override
	public int tripsCargados() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int stationsCargadas() {
		// TODO Auto-generated method stub
		return 0;
	}

	

}


