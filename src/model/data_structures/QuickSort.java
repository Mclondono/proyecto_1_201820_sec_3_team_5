package model.data_structures;

import java.util.Comparator;

public class QuickSort <T>
{ 
	Node head; 

	/* a node of the doubly linked list */  
	

	// A utility function to find last node of linked list     
	Node lastNode(Node node)
	{ 
		while(node.next!=null) 
			node = node.next; 
		return node; 
	} 


	/* Considers last element as pivot, places the pivot element at its 
   correct position in sorted array, and places all smaller (smaller than 
   pivot) to left of pivot and all greater elements to right of pivot */
	Node partition(Node l,Node h) 
	{ 
		// set pivot as h element 
		Comparable x = h.elem; 

		// similar to i = l-1 for array implementation 
		Node i = l.prev; 

		// Similar to "for (int j = l; j <= h- 1; j++)" 
		for(Node j=l; j!=h; j=j.next) 
		{ 
			if(j.elem.compareTo(x)<=0) 
			{ 
				// Similar to i++ for array 
				i = (i==null) ? l : i.next; 
				Comparable temp = i.elem; 
				i.elem = j.elem; 
				j.elem = temp; 
			} 
		} 
		i = (i==null) ? l : i.next;  // Similar to i++ 
		Comparable temp = i.elem; 
		i.elem = h.elem; 
		h.elem = temp; 
		return i; 
	} 
	
	/* A recursive implementation of quicksort for linked list */
	public void _quickSort(Node l,Node h ) 
	{ 
		if(h!=null && l!=h && l!=h.next){ 
			Node temp = partition(l,h); 
			_quickSort(l,temp.prev); 
			_quickSort(temp.next,h); 
		} 
	} 
	
	// The main function to sort a linked list. It mainly calls _quickSort() 
	public void quickSort(Node node) 
	{ 
		// Find last node 
		Node head = lastNode(node); 

		// Call the recursive QuickSort 
		_quickSort(node,head); 
	} 

	// A utility function to print contents of arr 
	public void printList(Node head) 
	{ 
		while(head!=null){ 
			System.out.print(head.elem+" "); 
			head = head.next; 
		} 
	} 

	/* Function to insert a node at the beginging of the Doubly Linked List */
	void push(int new_Data) 
	{ 
		Node new_Node = new Node(new_Data);     /* allocate node */

		// if head is null, head = new_Node 
		if(head==null){ 
			head = new_Node; 
			return; 
		} 

		/* link the old list off the new node */
		new_Node.next = head; 

		/* change prev of head node to new node */
		head.prev = new_Node; 

		/* since we are adding at the begining, prev is always NULL */
		new_Node.prev = null; 

		/* move the head to point to the new node */
		head = new_Node; 
	} 

} 