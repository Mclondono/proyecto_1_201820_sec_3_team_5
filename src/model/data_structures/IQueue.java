package model.data_structures;

import java.util.Iterator;

public interface IQueue<T extends Comparable<T>> extends Iterable<T>
{
	/**
	 * Agrega el elemento elem al final de la cola
	 * @param elem elemento a agregar
	 */
	public void enqueue(T elem);

	/**
	 * Elimina el primer elemento de la cola
	 * @return el elemento eliminado
	 */
	public T dequeue();
    
	/**
	 * Tama�o de la cola
	 * @return Tama�o de la cola
	 */
	public int getSize();
    
	/**
	 * Indica si la cola esta vacia
	 * @return true si la cola esta vacia, false de lo contrario
	 */
	public boolean isEmpty();
	
	/**
	 * Obtiene el primer nodo de la cola
	 * @return El primer nodo de la cola
	 */
	public Node<T> getFirst();
	
	/**
	 * Obtiene el ultimo nodo de la cola
	 * @return el ultimo nodo de la cola
	 */
	public Node<T> getLast();

    @Override
    public Iterator<T> iterator();

	
}
