package model.data_structures;

public interface IComparator<T> {
	

	public int compare(T objI, T objD);
	
}
