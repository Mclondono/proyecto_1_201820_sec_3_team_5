package model.data_structures;

import java.util.Iterator;

public class Stack <T extends Comparable <T>> extends DoublyLinkedList<T> implements IStack<T>
{
	@Override
	public Iterator<T> iterator() 
	{
		return first.iterator();
	}
	
	@Override
	public void push(T elem) 
	{
		addFirst(elem);		
	}

	@Override
	public T pop() 
	{
		return deleteFirst().get();
	}

	@Override
	public boolean isEmpty() 
	{
		return (first==null?true:false);
	}

	@Override
	public int getSize()
	{
		return size;
	}
	
	@Override
	public Node<T> getLast() 
	{
		return last;
	}

}
