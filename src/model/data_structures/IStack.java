package model.data_structures;

import java.util.Iterator;

public interface IStack<T extends Comparable<T>> extends Iterable<T> 
{
	/**
	 * Agrega el elemento elem al final de la cola
	 * @param elem Elemento a agregar
	 */
    public void push(T elem);

    /**
     * Elima el elemento mas reciente que se halla adicionado
     * @return El elemento eliminado
     */
    public T pop();

    /**
     * Tama�o de la cola
     * @return tama�o de la cola 
     */
    public int getSize();
    
    /**
     * Determina si la cola se encuentra vacia
     * @return True si la cola esta vacia, false de lo contrario
     */
    public boolean isEmpty();

    @Override
    public Iterator<T> iterator();
    
    /**
     * Da el ultimo nodo de la cola
     * @return Ultimo nodo de la cola
     */
	public Node<T> getLast();
}
