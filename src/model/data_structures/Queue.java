package model.data_structures;

import java.util.Iterator;

public class Queue <T extends Comparable<T>> extends DoublyLinkedList<T> implements IQueue<T>
{

	@Override
	public void enqueue(T elem) 
	{
		add(elem);		
	}
	
	@Override
	public int getSize() 
	{
		return size;
	}

	@Override
	public Iterator<T> iterator() 
	{
		return first.iterator();
	}
	
	@Override
	public T dequeue() 
	{
		T elem = first.get();
		remove(elem);
		return elem;
	}

	@Override
	public boolean isEmpty() 
	{
		if( first == null)
			return true;
		else
			return false;
	}

	@Override
	public Node<T> getLast() 
	{
		return last;
	}

	@Override
	public Node<T> getFirst() 
	{
		return first;
	}

}
