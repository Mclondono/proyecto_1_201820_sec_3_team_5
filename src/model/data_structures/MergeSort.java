package model.data_structures;
import java.util.Comparator;

import model.data_structures.Node;

public class MergeSort <T extends Comparable<T>>
{
    Node<T> merge(Node<T> first, Node<T> second, IComparator<T> comparator) 
    { 
        // If first linked list is empty 
        if (first == null) 
        { 
            return second; 
        } 
  
        // If second linked list is empty 
        if (second == null) 
        { 
            return first; 
        } 
  
        // Pick the smaller value 
        if (comparator.compare(first.elem, second.elem) <= 0 )
//        if (first.elem.compareTo(second.elem) <= 0) 
        { 
            first.next = merge(first.next, second, comparator); 
            first.next.prev = first; 
            first.prev = null; 
            return first; 
        } 
        else 
        { 
            second.next = merge(first, second.next, comparator); 
            second.next.prev = second; 
            second.prev = null; 
            return second; 
        } 
    }
    
    Node<T> split(Node<T> head) 
    { 
        Node<T> fast = head, slow = head; 
        while (fast.next != null && fast.next.next != null) 
        { 
            fast = fast.next.next; 
            slow = slow.next; 
        } 
        Node<T> temp = slow.next; 
        slow.next = null; 
        return temp; 
    } 
  
    public Node<T> mergeSort(Node<T> node, IComparator<T> comparator) 
    { 
        if (node == null || node.next == null) 
        { 
            return node; 
        } 
        Node<T> second = split(node); 
  
        // Recur for left and right halves 
        node = mergeSort(node, comparator); 
        second = mergeSort(second, comparator); 
  
        // Merge the two sorted halves 
        return merge(node, second, comparator); 
    } 
}
