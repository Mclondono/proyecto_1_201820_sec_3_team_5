package model.vo;

import java.time.LocalDateTime;

import model.data_structures.IComparator;
public class VOTrip implements Comparable<VOTrip> 
{
	/**
	 * Indica si el usuario es un hombre
	 */
	public final static String MALE = "male";

	/**
	 * Indica si el usuario es un amujer
	 */
	public final static String FEMALE = "female";

	/**
	 * Indica que no se especifica que genero es el usuario
	 */
	public final static String UNKNOWN = "unknown";

	/**
	 * ID del trip
	 */
	private int tripId;

	/**
	 * Tiempo de inicio 
	 */
	private LocalDateTime startTime;

	/**
	 * Tiempo de finalizacion
	 */
	private LocalDateTime stopTime;

	/**
	 * ID de la bicicleta
	 */
	private int bikeId;

	/**
	 * Duracion del viaje
	 */
	private double tripDuration;

	/**
	 * ID de la estacion de inicio
	 */
	private int startStationId;

	/**
	 * ID de la estacion de fin
	 */
	private int endStationId;

	/**
	 * Genero del usuario
	 */
	private String gender;

	/**
	 * Distancia del viaje
	 */
	private Double distance;

	private boolean stationfC4 ;

	/**
	 * Crea un viaje con la informacion que entra por parametro
	 * @param tripId ID del viaje
	 * @param startTime Tiempo de incio del viaje
	 * @param stopTime Tiempo de finalizacion del viaje
	 * @param bikeId ID de la bicicleta 
	 * @param tripDuration Duracion del viaje
	 * @param startStationId ID de la estacion de incio
	 * @param endStationId ID de la estacion de finalizacion
	 * @param gender Genero del usuario
	 * @param distance Distancia del viaje
	 */
	public VOTrip(String tripId, String startTime, String stopTime, String bikeId, String tripDuration, int startStationId, 
			int endStationId, String gender, Double distance) 
	{
		this.tripId = Integer.parseInt(tripId);
		this.startTime = convertirFecha_Hora_Date(startTime);
		this.stopTime = convertirFecha_Hora_Date(stopTime);
		this.bikeId = Integer.parseInt(bikeId);
		this.tripDuration = Double.parseDouble(tripDuration);
		this.startStationId = startStationId;
		this.endStationId = endStationId;
		this.gender = gender;
		this.distance = distance;
		stationfC4=false;
	}

	public void changec4(boolean valor)
	{
			stationfC4 = valor;
		
	}

	public boolean isStationfC4() {
		return stationfC4;
	}

	@Override
	public int compareTo(VOTrip o) 
	{
		return startTime.compareTo(o.startTime);
	}

	/**
	 * ID del viaje
	 * @return ID del viaje
	 */
	public int getTripId() 
	{
		return tripId;
	}

	/**
	 * Tiempo de inicio del viaje
	 * @return Tiempo de inicio
	 */
	public LocalDateTime getStartTime() 
	{
		return startTime;
	}

	/**
	 * Tiempo de finalizacion del viaje
	 * @return Tiempo de finalizacion
	 */
	public LocalDateTime getStopTime() 
	{
		return stopTime;
	}

	/**
	 * ID de la bicicleta
	 * @return ID bicicleta
	 */
	public int getBikeId() 
	{
		return bikeId;
	}

	/**
	 * Distancia del viaje
	 * @return Distncia viaje
	 */
	public double getTripDistance() 
	{
		return distance;
	}

	/**
	 * Duracion del viaje
	 * @return Duracion viaje
	 */
	public double getTripDuration() 
	{
		return tripDuration;
	}

	/**
	 * ID de la estacion de inicio
	 * @return ID estacion inicio
	 */
	public int getStartStationId() 
	{
		return startStationId;
	}

	/**
	 * ID de la estacion de finalizacion
	 * @return ID estacion final
	 */
	public int getEndStationId() 
	{
		return endStationId;
	}

	/**
	 * Genero del usuario
	 * @return Usuario genero
	 */
	public String getGender() 
	{
		return gender;
	}

	/**
	 * Convertir fecha y hora a un objeto LocalDateTime
	 * @param fecha fecha en formato dd/mm/aaaa con dd para dia, mm para mes y aaaa para agno
	 * @param hora hora en formato hh:mm:ss con hh para hora, mm para minutos y ss para segundos
	 * @return objeto LDT con fecha y hora integrados
	 */
	private LocalDateTime convertirFecha_Hora_Date(String date)
	{
		String[] fechaHora = date.split(" ");
		String fecha = fechaHora[0];
		String hora = fechaHora[1];

		String[] datosFecha = fecha.split("/");
		String[] datosHora = hora.split(":");

		int agno = Integer.parseInt(datosFecha[2]);
		int mes = Integer.parseInt(datosFecha[0]);
		int dia = Integer.parseInt(datosFecha[1]);
		int horas = Integer.parseInt(datosHora[0]);
		int minutos = Integer.parseInt(datosHora[1]);

		if(datosHora.length == 3)
		{
			int segundos = Integer.parseInt(datosHora[2]);
			return LocalDateTime.of(agno, mes, dia, horas, minutos, segundos);
		}
		else

		{
			return LocalDateTime.of(agno, mes, dia, horas, minutos);
		}
	}

	public static class ComparadorXHoraEnd implements IComparator<VOTrip>
	{
		public int compare(VOTrip t1, VOTrip t2)
		{
			return t1.getStopTime().compareTo(t2.getStopTime());
		}
	}
	public static class ComparadorXHoraEndXhoraInical implements IComparator<VOTrip>
	{
		public int compare(VOTrip t1, VOTrip t2)
		{
			if(t1.stationfC4&&t2.stationfC4)
				return t1.getStopTime().compareTo(t2.getStopTime());
			else if(t1.stationfC4&&!t2.stationfC4)
				return t1.getStopTime().compareTo(t2.getStartTime());
			else if(!t1.stationfC4&&t2.stationfC4)
				return t1.getStartTime().compareTo(t2.getStopTime());
			else
				return t1.getStartTime().compareTo(t2.getStartTime());				
		}
	}
}
