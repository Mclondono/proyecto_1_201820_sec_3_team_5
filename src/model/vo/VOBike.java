package model.vo;

import java.time.LocalDateTime;
import java.util.Iterator;

import model.data_structures.DoublyLinkedList;
import model.data_structures.IComparator;
import model.data_structures.QuickSort;

public class VOBike implements Comparable<VOBike>
{
	/**
	 * ID de la bicicleta
	 */
    private int VOBikeId;
    
    /**
     * Numero total de viajes
     */
    private int totalVOTrips;
    
    /**
     * Distancia recorrida
     */
    private int totalDistance;
    
    /**
     * Tiempo de uso
     */
    private int totalDuration;
    
    /**
     * Lista de los viajes de la bicicleta
     */
    private DoublyLinkedList<VOTrip> listaViajes;
    
    /**
     * Duracion total de uso
     */
    private int duracion;

    /**
     * Crea una bicicleta
     * @param VOBikeId ID de la bicicleta
     */
    public VOBike(int VOBikeId ) 
    {
        this.VOBikeId = VOBikeId;
        this.totalVOTrips = 0;
        this.totalDistance = 0;
        this.totalDuration = 0;
        listaViajes = new DoublyLinkedList<VOTrip>();
    }

    @Override
    public int compareTo(VOBike o) 
    {
    	if( VOBikeId < o.VOBikeId)
    		return -1;
    	else if(VOBikeId == o.VOBikeId)
    		return 0;
    	else
    		return 1;
    }

    /**
     * Da el ID de la bicicleta
     * @return VOBikeId
     */
    public int getVOBikeId() 
    {
        return VOBikeId;
    }

    /**
     * Da el total de viajes
     * @return Total de viajes
     */
    public int getTotalVOTrips() 
    {
    	return totalVOTrips;
    }

    /**
     * Da la distancia total recorrida
     * @return distancia total recorrida
     */
    public int getTotalDistance() 
    {
        return totalDistance;
    }
    
    /**
     * Da el tiempo de uso
     * @return Tiempo total de uso
     */
    public int getTotalDuration() 
    {
    	return totalDuration;
    }
    
    /**
     * Anade un viaje a la bicicleta
     * @param nuevoTp
     */
    public void anadirviaje( VOTrip nuevoTp )
    {
    	listaViajes.addFirst(nuevoTp);
    }
    
    public void ordenarViajes()
    {
    	QuickSort<VOTrip> quick = new QuickSort<VOTrip>();
    	quick.quickSort(listaViajes.getFirst());
    	
    }
    
    public DoublyLinkedList<VOTrip> viajesPorFecha(LocalDateTime initial, LocalDateTime end)
    {
    	DoublyLinkedList<VOTrip> i = new DoublyLinkedList<VOTrip>();
    	Iterator<VOTrip> iter = listaViajes.iterator();
    	totalDistance = 0;
    	while( iter.hasNext())
    	{
    		VOTrip actual = (VOTrip) iter.next();
    		if ( actual.getStartTime().isAfter(initial) && actual.getStopTime().isBefore(end) )
    		{
    			i.add(actual);
    			totalDistance += actual.getTripDistance();
    		}
    	}
    	totalVOTrips = i.size();
    	return i;
    }
    
    public DoublyLinkedList<VOTrip> getListaViajes() {
		return listaViajes;
	}

    public int getDuration()
    {
    	return duracion;
    }
    
    public void calcularTotalDeUso()
    {
    	int tiempo = 0;
    	Iterator<VOTrip> iter = listaViajes.iterator();
    	while( iter.hasNext() )
    	{
    		VOTrip actual = iter.next();
    		tiempo += actual.getTripDuration();
    	}
    	duracion = tiempo;
    }
    
    public static class ComparadorVOBikeXTotalUso implements IComparator<VOBike>
    {
    	public int compare(VOBike b1, VOBike b2)
    	{
    		if(b1.duracion < b2.duracion )
    		{
    			return -1;
    		}
    		else if ( b1.duracion == b2.duracion )
    		{
    			return 0;
    		}
    		else
    		{
    			return 1;
    		}
    	}
    }
    
    public static class ComparadorVOBikeXNumeroViajes implements IComparator<VOBike>
    {
    	public int compare(VOBike b1, VOBike b2)
    	{
    		if(b1.totalVOTrips < b2.totalVOTrips )
    		{
    			return -1;
    		}
    		else if ( b1.totalVOTrips == b2.totalVOTrips )
    		{
    			return 0;
    		}
    		else
    		{
    			return 1;
    		}
    	}
    }
    
    public static class ComparadorVOBikeXDistancia implements IComparator<VOBike>
    {
    	public int compare(VOBike b1, VOBike b2)
    	{
    		if(b1.totalDistance < b2.totalDistance )
    		{
    			return -1;
    		}
    		else if ( b1.totalDistance == b2.totalDistance )
    		{
    			return 0;
    		}
    		else
    		{
    			return 1;
    		}
    	}
    }
}
