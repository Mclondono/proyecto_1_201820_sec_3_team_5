package model.vo;

import java.time.LocalDateTime;

public class VOStation implements Comparable<VOStation> 
{
	/**
	 * ID de la estacion
	 */
	private int stationId;
	
	/**
	 * Nombre de la estacion
	 */
	private String stationName;
	
	/**
	 * Time cuando la estacion empezo a funcionar
	 */
	private LocalDateTime startDate;
	
	/**
	 * latitud de la estacion
	 */
	private double latitude;
	
	/**
	 * longitud de la estacion
	 */
	private double longitude;
	
	/**
	 * Capacidad de la estacion
	 */
	private int capacity;

	/**
	 * Crea una nueva estacion con la informacion que entra por parametro
	 * @param sStationId id de la estacion
	 * @param sStationName nombre de la estacion
	 * @param sLatitude latitud de la estacion
	 * @param sLongitude longitud de la estacion
	 * @param sCapacity capacidad de la estacion
	 * @param sStartDate Tiempo de incicio de operacion
	 */
	public VOStation(String sStationId, String sStationName, String sLatitude, String sLongitude, String sCapacity,
			String sStartDate) 
	{
		stationId = Integer.parseInt(sStationId);
		stationName = sStationName;
		latitude = Double.parseDouble(sLatitude);
		longitude = Double.parseDouble(sLongitude);
		capacity =  Integer.parseInt(sCapacity);
		startDate = convertirFecha_Hora_LDT(sStartDate);

	}

	/**
	 * Latitud de la estacion
	 * @return latitud de la estacion
	 */
	public double getLatitude() 
	{
		return latitude;
	}

	/**
	 * Longitud de la estacion
	 * @return Longitud de la estacion
	 */
	public double getLongitude() 
	{
		return longitude;
	}

	/**
	 * Capacidad de la estacion
	 * @return Capacidad de la estacion
	 */
	public int getCapacity() 
	{
		return capacity;
	}

	@Override
	public int compareTo(VOStation o) 
	{
		return startDate.compareTo(o.startDate);
	}

	/**
	 * Tiempo de inicio de operacion
	 * @return Tiempo de inicio de operacion
	 */
	public LocalDateTime getStartDate() 
	{
		return startDate;
	}

	/**
	 * ID de la estacion
	 * @return ID de la estacion
	 */
	public int getStationId() 
	{
		return stationId;
	}

	/**
	 * Nombre de la estacion
	 * @return Nombre de la estacion
	 */
	public String getStationName() 
	{
		return stationName;
	}
	
	/**
	 * Convertir fecha y hora a un objeto LocalDateTime
	 * @param fecha fecha en formato dd/mm/aaaa con dd para dia, mm para mes y aaaa para agno
	 * @param hora hora en formato hh:mm:ss con hh para hora, mm para minutos y ss para segundos
	 * @return objeto LDT con fecha y hora integrados
	 */
	private LocalDateTime convertirFecha_Hora_LDT(String date)
	{
		String[] fechaHora = date.split(" ");
		String fecha = fechaHora[0];
		String hora = fechaHora[1];
		
		String[] datosFecha = fecha.split("/");
		String[] datosHora = hora.split(":");

		int agno = Integer.parseInt(datosFecha[2]);
		int mes = Integer.parseInt(datosFecha[0]);
		int dia = Integer.parseInt(datosFecha[1]);
		int horas = Integer.parseInt(datosHora[0]);
		int minutos = Integer.parseInt(datosHora[1]);
		
		if(datosHora.length == 3)
		{
			int segundos = Integer.parseInt(datosHora[2]);
			return LocalDateTime.of(agno, mes, dia, horas, minutos, segundos);
		}
		else
		{
			return LocalDateTime.of(agno, mes, dia, horas, minutos);
		}
	}
}
