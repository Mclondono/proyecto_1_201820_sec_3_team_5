package controller;

import model.data_structures.IQueue;
import model.data_structures.IDoublyLinkedList;
import model.data_structures.IDoublyLinkedList;
import model.logic.DivvyTripsManager;
import model.vo.VOBike;
import model.vo.VOStation;
import model.vo.VOTrip;
import view.DivvyTripsManagerView;

import java.time.LocalDateTime;

import api.IDivvyTripsManager;


public class Controller 
{	
	
    private static IDivvyTripsManager manager = new DivvyTripsManager();

    public static IQueue<VOTrip> A1(LocalDateTime fechaInicial, LocalDateTime fechaFinal)
    {
        return manager.A1ViajesEnPeriodoDeTiempo(fechaInicial, fechaFinal);
    }

    public static IDoublyLinkedList<VOBike> A2(LocalDateTime fechaInicial, LocalDateTime fechaFinal)
    {
        return manager.A2BicicletasOrdenadasPorNumeroViajes(fechaInicial, fechaFinal);
    }
    
   
    public IDoublyLinkedList<VOTrip> A3(int VOBikeId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) 
    {
        return manager.A3ViajesPorBicicletaPeriodoTiempo(VOBikeId, fechaInicial, fechaFinal);
    }

    
    public IDoublyLinkedList<VOTrip> A4(int endVOStationId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) 
    {
        return manager.A4ViajesPorEstacionFinal(endVOStationId, fechaInicial, fechaFinal);
    }

   
    public IQueue<VOStation> B1(LocalDateTime fechaComienzo) 
    {
        return manager.B1EstacionesPorFechaInicioOperacion(fechaComienzo);
    }

   
    public IDoublyLinkedList<VOBike> B2(LocalDateTime fechaInicial, LocalDateTime fechaFinal) 
    {
        return manager.B2BicicletasOrdenadasPorDistancia(fechaInicial, fechaFinal);
    }

  
    public IDoublyLinkedList<VOTrip> B3(int VOBikeId, int tiempoMaximo, String genero) 
    {
        return manager.B3ViajesPorBicicletaDuracion(VOBikeId, tiempoMaximo, genero);
    }

    
    public IDoublyLinkedList<VOTrip> B4(int startVOStationId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) 
    {
        return manager.B4ViajesPorEstacionInicial(startVOStationId, fechaInicial, fechaFinal);
    }
    
	
    public static void C1cargar(String dataVOTrips, String dataVOStations)
    {
    	manager.C1cargar(dataVOTrips, dataVOStations);
    }
    
    
    public int tripsCargados()
    {
    	return manager.tripsCargados();
    }
    
    public int stationsCargadas()
    {
    	return manager.stationsCargadas();
    }
    
    
    public IQueue<VOTrip> C2ViajesValidadosBicicleta(int VOBikeId, LocalDateTime fechaInicial, LocalDateTime fechaFinal)
    {
    	return manager.C2ViajesValidadosBicicleta(VOBikeId, fechaInicial, fechaFinal);
    }
    
   
    public  IDoublyLinkedList<VOBike> C3BicicletasMasUsadas(int topBicicletas)
    {
		return manager.C3BicicletasMasUsadas(topBicicletas);
    }
    
    
    public IDoublyLinkedList<VOTrip> C4ViajesEstacion(int VOStationId, LocalDateTime fechaInicial, LocalDateTime fechaFinal)
    {
    	return manager.C4ViajesEstacion(VOStationId, fechaInicial, fechaFinal);
    }
}
